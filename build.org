* branches

** 0


| 10 | checkpoints |
|    |             |
| 20 | debian      |
|    |             |


** 10


| at master                      |
| build and test                 |
| branch and checkout new branch |
| docker hub build image         |
| go back master branch          |
|                                |



** 20

|          |               |    |         | stable-slim            |
|          |               |    |         |                        |
| jessie   | jessie-slim   |  8 | 8-slim  |                        |
|          |               |    |         |                        |
| stretch  | stretch-slim  |  9 | 9-slim  |                        |
|          |               |    |         |                        |
| buster   | buster-slim   | 10 | 10-slim | [2020-05-11 Mon 16:43] |
|          |               |    |         |                        |
| bullseye | bullseye-slim | 11 |         |                        |
|          |               |    |         |                        |


https://en.wikipedia.org/wiki/Debian_version_history


https://hub.docker.com/_/debian?tab=description&page=1&ordering=-name




** 30


|          | jessie | stretch          | buster |
|          |     80 | 90               |    100 |
|          |        |                  |        |
| pcsc     |        | stretch-pcsc     |        |
|          |        |                  |        |
| ezmini   |        | stretch-ezmini   |        |
|          |        |                  |        |
| gpki     |        | stretch-gpki     |        |
|          |        |                  |        |
| chromium |        | stretch-chromium |        |
|          |        |                  |        |
| firefox  |        | stretch-firefox  |        |
|          |        |                  |        |
| nhiicc   |        | stretch-nhiic    |        |
|          |        |                  |        |


* pcsc


* ezmini


* gpki


* chromium


* firefox


* nhiicc


* flow


** 0


| 10 | stretch |
|    |         |


** 10


| at stretch                     | M-x magit-branch-and-checkout stretch |
| build and test                 |                                       |
| branch and checkout new branch |                                       |
| docker hub build image         |                                       |
| go back master branch          |                                       |
|                                |                                       |





